const mongoose = require('mongoose');


const formSchema = mongoose.Schema({
    lastname : { type: String, required : true},
    firstname : { type: String, required : true},
    email : { type: String, required : true},
    message : { type: String, required : true},
});

module.exports = mongoose.model('Form', formSchema)