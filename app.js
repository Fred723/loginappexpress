var express = require("express");
var app = express();
const bcrypt = require('bcrypt');

var cors = require('cors');
app.use(cors());


const methodOverride = require('method-override')

const Form = require("./models/Form");
const User = require("./models/User");

app.use(methodOverride('_method'))
var bodyParser = require("body-parser");
const { default: mongoose } = require("mongoose");
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({extended: false}));
require('dotenv').config();

const url = process.env.DATABASE_URL

const connectionParams ={
    useNewUrlParser:true,
    useUnifiedTopology: true
}

mongoose.connect(url, connectionParams).then(()=>{
    console.log("Mongodb database connected !");
}).catch(err => console.log(err));


app.get("/", function (req, res){
    // res.send("<html><body><h1>Hello World</h1></body></html>");
    // res.sendFile("/Users/frede/Documents/formation dev web/cours/NodeJS-express/dev1/index.html");
    //res.sendFile("/Users/frede/Documents/formation dev web/cours/NodeJS-express/dev1/form.html");
    //res.render("Home");
    Form.find().then(data =>{
        res.json(data);
        // res.render('Home', {data:data});
    }).catch(err=>console.log(err));

})

app.post('/api/register', function(req, res){
    const Data = new User({
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        admin: false
    })

    Data.save().then(()=>{
        console.log("User saved !");
        res.redirect('/');
    });
});

app.get('/register', function(req, res){
    res.render("Register");
});

app.get('/login', function(req, res){
    res.render("Login");
});

app.post("/api/login", function(req,res){
    User.findOne({
        email: req.body.email
     }).then(user =>{
        if(!user){
           return res.status(404).send("No user found !");
        }
        if(!bcrypt.compareSync(req.body.password, user.password)){
            return res.status(404).send("Invalid password");
        }

        // if(user.password != req.body.password){
        //     return res.status(404).send("haha tu t'es trompé de mot de passe");
        // }
        res.render('UserPage', {data:user});
     }).catch(err => console.log(err));
});




app.post("/submit-data-form", function(req, res){
    const Data = new Form({
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        email: req.body.email,
        message: req.body.message
    })

    Data.save().then(()=>{
        console.log("Data saved !");
        res.redirect('http://localhost:3000/');
    });
});

app.get("/form/:id", (req,res)=>{
    Form.findOne({
       _id: req.params.id
    }).then(data =>{
        res.json(data);
        // res.render('Page', {data:data});
    }).catch(err => console.log(err));
})

app.get("/form/edit/:id", (req,res)=>{
    Form.findOne({
       _id: req.params.id
    }).then(data =>{
        res.render('Edit', {data:data});
    }).catch(err => console.log(err));
})


app.put("/form/edit/:id", (req, res)=>{
    Form.findOne({
        _id: req.params.id
     }).then(data =>{
        data.lastname= req.body.lastname,
        data.firstname= req.body.firstname,
        data.email= req.body.email,
        data.message= req.body.message

        data.save().then(()=>{
            console.log("Data changed !");
            res.redirect('/');
        }).catch(err => console.log(err));

     }).catch(err => console.log(err));
})

app.delete("/form/delete/:id",(req, res)=>{
    Form.remove({
        _id: req.params.id
    }).then(()=>{
        console.log("data deleted");
        res.redirect("/");
    }).catch(err => console.log(err))
})

const port = process.env.PORT || 5000;


var server = app.listen(port, function(){
    console.log("Node server is running");
});

